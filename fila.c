#include "fila.h"

void FFVazia(TipoFila *Fila) {
    int i;
    for(i = 0; i < 7; i++) {
        Fila->sujeito[i].id = -1;
        Fila->sujeito[i].nome = "vazio";
    }
}

bool Vazia(TipoFila *Fila) {
    int i;
    for(i = 0; i < 7; i++) {
        if (Fila->sujeito[i].id != -1) {
            return false;
        }
    }
    return true;
}

void Enfileira(personagem *a, TipoFila *Fila) {
    Fila->sujeito[a->id].id = a->id;
    Fila->sujeito[a->id].nome = a->nome;
}

void Desenfileira(TipoFila *Fila, int id) {
    if (Vazia(Fila)) {
        puts("[ERRO] Fila vazia");
        return;
    }
    Fila->sujeito[id].id = -1;
    Fila->sujeito[id].nome = "vazio";
}

void Imprime(TipoFila *fila) {
    int i;
    for(i = 0; i < 7; i++) {
        printf("%d\n", fila->sujeito[i].id);
    }
    fflush(stdout);
}
