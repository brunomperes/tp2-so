#include <stdio.h>
#include <stdlib.h>

#include "fila.h"
#include "threads.h"
#include "forno.h"

/**
 Variáveis globais
*/
pthread_cond_t personagens[3];
pthread_mutex_t forno;
pthread_mutex_t usando_forno;
pthread_mutex_t mutex_Fila;
int quantidadeUsos; //Quantidade de vezes que o forno é usado por uma pessoa
int totalUsosForno;
TipoFila fila;

int main (int argc, char *argv[]) {

    pthread_t thread[7];
    int num_threads = 7;
    int i;

    quantidadeUsos = atoi(argv[1]);

    if (quantidadeUsos < 0) {
        perror ("Quantidade de usos do forno inválida");
    }

    totalUsosForno = quantidadeUsos * 6;

    srand (time(NULL));

    personagem **a;
    a = inicializaPersonagens(quantidadeUsos);

    //Inicializa os status de namorados
    for(i = 1 ; i < 7 ; i += 2) {
        definirCasal(a[i], a[i + 1]);
    }

    for(i = 1 ; i < 7 ; i++) {
        if(i % 2 == 1 ) {
            //Define prioridade dos homens
            definirPrioridade(a[(i + 2) % 6], a[i]);

        } else {
            definirPrioridade(a[i], a[(i + 3) % 6]);
        }

    }

    //Inicializa o mutex do forno
    pthread_mutex_init(&forno, NULL);
    pthread_mutex_init(&usando_forno, NULL);
    pthread_mutex_init(&mutex_Fila, NULL);

    pthread_cond_init(&personagens[0], NULL);
    pthread_cond_init(&personagens[1], NULL);
    pthread_cond_init(&personagens[2], NULL);

    //Inicializa a fila de espera
    FFVazia(&fila);

    pthread_create(&(thread[0]), NULL, raj, (void*)(a[0]));
    for(i = 1 ; i < num_threads ; i++) {
        pthread_create(&(thread[i]), NULL, diaAdiaPersonagem, (void*)(a[i]));
    }

    // Espera que o fim das threads
    for(i = 1; i < num_threads; i++) {
        pthread_join(thread[i], NULL);
    }

    //Destroi o mutex do forno
    pthread_mutex_destroy(&forno);
    pthread_mutex_destroy(&usando_forno);
    pthread_mutex_destroy(&mutex_Fila);

    pthread_cond_destroy(&personagens[0]);
    pthread_cond_destroy(&personagens[1]);
    pthread_cond_destroy(&personagens[2]);

    return 0;
}
