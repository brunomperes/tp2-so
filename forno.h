#ifndef FORNO_H_INCLUDED
#define FORNO_H_INCLUDED

typedef struct personagem {

    int id;
    int quantUsos;
    char *nome;
    struct personagem *prioridadeHomen; //Aponta para a pessoa com prioridade minimamente maior que a atual
    struct personagem *prioridadeMulher;
    struct personagem *namorados; //Referência para o namorado(a) do personagem

} personagem;

void usaForno(personagem *a);

void esperaAleatoria();

int idCorreta(int id);

void esperarForno(personagem *a);

// Faz o personagem querer usar o forno em intervalos de tempos aleatórios
void *diaAdiaPersonagem(void *threadid);

void libera(personagem *a);

int liberaAlguem();

void *raj(void *threadid);


#endif // FORNO_H_INCLUDED
