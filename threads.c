#include "threads.h"

personagem *construtor(int id, char *nome, int quantidadeUsos) { //constrois os personagens e passa para a estrutura o id e o nome.
    personagem *x;
    x = malloc(sizeof(personagem));
    x->id = id;
    x->nome = nome;
    x->quantUsos = quantidadeUsos;
    x->prioridadeHomen = NULL;
    x->prioridadeMulher = NULL;
    x->namorados = NULL;
    return x;
}

//Define que o personagem A e sua namorada tem prioridade menor q a de B
void definirPrioridade(personagem *a, personagem *b) {
    a->prioridadeHomen = b;
    a->prioridadeMulher = b->namorados;
}

void definirCasal(personagem *a, personagem *b) {
    a->namorados = b;
    b->namorados = a;
}

personagem **inicializaPersonagens(int quantidadeUsos) { //inicializa os personagens
    char *personagens[7] = {"Raj", "Sheldon", "Amy", "Howard", "Bernadette", "Leonard" , "Penny"};
    personagem **a;
    int i;
    a = (personagem**)malloc(sizeof(personagem) * 7); //alocar dinamicamente.

    for(i = 0 ; i < 7 ; i ++) {
        a[i] = (personagem*)malloc(sizeof(personagem) * 2);
    }

    for(i = 0 ; i < 7 ; i++) {
        a[i] = construtor(i, personagens[i], quantidadeUsos); //seta o id e o nome de cada personagem i
    }

    return a; //retorna o vetor de personagens cada um com seu id e nome.
}
