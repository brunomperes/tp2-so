#include "fila.h"
#include "forno.h"

#define TEMPO_USO_FORNO 1
#define TEMPO_ESPERA_MIN 3
#define TEMPO_ESPERA_MAX 6

#include <pthread.h>

extern pthread_mutex_t forno;
extern pthread_mutex_t mutex_Fila;
extern pthread_cond_t personagens[3];

extern int quantidadeUsos; //Quantidade de vezes que o forno é usado por uma pessoa
extern TipoFila fila;
extern int totalUsosForno;

int idUsandoForno;
int deadlock = 0;

void usaForno(personagem *a) {

    if (a == NULL) {
        perror("[ERRO] valor invalido recebido em usaForno()");
    }

    printf("%s comeca a esquentar algo\n", a->nome);
    fflush(stdout);

    sleep(TEMPO_USO_FORNO);

    printf("%s vai comer\n", a->nome);
    fflush(stdout);

    totalUsosForno--;
    pthread_mutex_unlock(&forno);

    pthread_mutex_lock(&mutex_Fila);
    Desenfileira(&fila, a->id);
    libera(a);
    pthread_mutex_unlock(&mutex_Fila);
}

void esperaAleatoria() {
    int tempoEspera = (rand() % (TEMPO_ESPERA_MAX - TEMPO_ESPERA_MIN + 1)) + TEMPO_ESPERA_MIN;
    sleep(tempoEspera);
}

void esperarForno(personagem *a) {

    pthread_mutex_lock(&forno);

    if(a->id == 1 || a->id == 2) {
        pthread_cond_wait(&personagens[0], &forno);
    } else if(a->id == 3 || a->id == 4) {
        pthread_cond_wait(&personagens[1], &forno);
    } else if(a->id == 5 || a->id == 6) {
        pthread_cond_wait(&personagens[2], &forno);
    } else {
        puts("ERRO");
    }

    pthread_mutex_unlock(&forno);
}

// Libera o forno a partir de um personagem que está usando o forno
void libera(personagem *a) {

    int contador = 0;
    int id;

    if (a == NULL || a->prioridadeHomen == NULL || a->prioridadeMulher == NULL) {
        perror("a recebeu um valor invalido em usaForno()");
    }

    if(fila.sujeito[a->prioridadeHomen->id].id != -1 || fila.sujeito[a->prioridadeMulher->id].id != -1) { // libera o personagem com maior prioridade ou a namorada do mesmo.
        //Procura o personagem de maior prioridade na fila
        while((fila.sujeito[a->prioridadeHomen->id].id != -1 || fila.sujeito[a->prioridadeMulher->id].id != -1) && deadlock != 1) {
            if(fila.sujeito[a->prioridadeHomen->id].id != -1) {
                a = a->prioridadeHomen;
            } else {
                a = a->prioridadeMulher;
            }

            if(contador > 3) {
                deadlock = 1;
            }
            contador++;
        }

        // Só libera se não tiver em deadlock
        if (deadlock != 1) {
            id = idCorreta(a->id);
            pthread_cond_signal(&personagens[id]);
        }

    } else if(fila.sujeito[a->namorados->id].id != -1) { //libera a namorada na fila
        fflush(stdout);
        id = idCorreta(a->id);
        pthread_cond_signal(&personagens[id]);
    } else {
        liberaAlguem();
    }

}

//Retorna a id do personagem para sinalizar a thread
int idCorreta(int id) {

    int i;

    if(id == 1 || id == 2) {
        i = 0;
    } else if(id == 3 || id == 4) {
        i = 1;
    } else if( id == 5 || id == 6) {
        i = 2;
    }

    return i;
}

int liberaAlguem() {
    int i = 1;
    int id;
    if (Vazia(&fila)) {
        return -1;
    }
    while(i < 7) {
        if(fila.sujeito[i].id != -1) {
            id = idCorreta(i);
            pthread_cond_signal(&personagens[id]);
            return i;
        }
        i++;
    }
    return -1;
}

void *raj(void *threadid) {
    while(totalUsosForno > 0) {
        sleep(5);
//        printf("Raj está verificando, total de usos do forno = %d\n", totalUsosForno);
        if(deadlock == 1) {
            int idLiberado = liberaAlguem();
            printf("Raj detectou um deadlock, e vai liberar %s\n", fila.sujeito[idLiberado].nome);
            deadlock = 0;
        }
    }
    pthread_exit(NULL);
}

// Faz o personagem querer usar o forno em intervalos de tempos aleatórios
void *diaAdiaPersonagem(void *threadid) {

    personagem *a = (personagem*)threadid;

    while(a->quantUsos > 0) {

        esperaAleatoria();

        printf("%s quer usar o forno\n", a->nome);
        fflush(stdout);

        //Entra na fila
        pthread_mutex_lock(&mutex_Fila);
        Enfileira(a, &fila);
        pthread_mutex_unlock(&mutex_Fila);


//         Se o forno está liberado
        if (pthread_mutex_trylock(&forno) == 0) {
            usaForno(a);
        } else {
            // Espera o forno ser liberado e usa
            esperarForno(a);
            usaForno(a);
        }

        a->quantUsos--;

    }

    pthread_exit(NULL);
}
