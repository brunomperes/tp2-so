#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

#include "forno.h"
#include "fila.h"

#ifndef THREADS_H_INCLUDED
#define THREADS_H_INCLUDED


void *chamaForno(void *threadid);

void definirPrioridade(personagem *a, personagem *b);

void definirCasal(personagem *a, personagem *b);

personagem *construtor(int id, char *nome, int quantidadeUsos);

personagem **inicializaPersonagens(int quantidadeUsos);


#endif // THREADS_H_INCLUDED
