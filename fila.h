#ifndef FILA_H_INCLUDED
#define FILA_H_INCLUDED

#include "threads.h"
#include <stdbool.h>

typedef struct TipoFila {
    personagem sujeito[7];
} TipoFila;

void FFVazia(TipoFila *Fila);

void Enfileira(personagem *a, TipoFila *Fila);

void Desenfileira(TipoFila *Fila, int id);

bool Vazia(TipoFila *Fila);

void Imprime(TipoFila *fila);

#endif // FILA_H_INCLUDED
